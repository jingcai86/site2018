import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilService {
  constructor() { }

  openUrl(url: string, seachingOnGoogle: boolean) {
    if (seachingOnGoogle) {
      window.open('https://www.google.com/search?q=' + url + '&ie=utf-8&oe=utf-8', '_blank');
    } else {
      window.open(url, '_blank');
    }
  }
}
