import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  getHeaderInfoContent(): Observable<object> {
    return this.http.get('data/header.info.json');
  }
  getHeaderIconContent(): Observable<object> {
    return this.http.get('data/header.icon.json');
  }

  getEducationContent(): Observable<object> {
    return this.http.get('data/education.info.json');
  }

  getPersonalLifeContent(): Observable<object> {
    return this.http.get('data/life.info.json');
  }

  getPositionContent(): Observable<object> {
    return this.http.get('data/position.info.json');
  }

  getSkillContent(): Observable<object> {
    return this.http.get('data/skill.info.json');
  }

  getAboutContent(): Observable<object> {
    return this.http.get('data/about.info.json');
  }
  getWorkContent(): Observable<object> {
    return this.http.get('data/work.info.json');
  }
  getSetupContent(): Observable<object> {
    return this.http.get('data/setup.info.json');
  }
}
