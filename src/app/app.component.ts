import { routerTransition } from './app.route.transition';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [ routerTransition ]
})
export class AppComponent {
  navLinks: object[];

  constructor() {
    this.navLinks = [
      {
          label: 'about',
          path: './about'
      }, {
          label: 'experience',
          path: './experience'
      }, {
          label: 'work',
          path: './work'
      }, {
          label: 'life',
          path: './life'
      }, {
          label: 'setup',
          path: './setup'
      }
    ];
  }

  getState(outlet): any {
    return outlet.activatedRouteData.state || 'about';
  }
}
