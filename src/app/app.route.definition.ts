import { RouterModule, Routes } from '@angular/router';

import { AboutComponent } from './about/about.component';
import { ExperienceComponent } from './experience/experience.component';
import { WorkComponent } from './work/work.component';
import { LifeComponent } from './life/life.component';
import { SetupComponent } from './setup/setup.component';

const appRoutes: Routes = [
    { path: 'about', component: AboutComponent, data: { state: 'about'}},
    { path: 'experience', component: ExperienceComponent, data: { state: 'experience'}},
    { path: 'work', component: WorkComponent, data: { state: 'work'}},
    { path: 'life', component: LifeComponent, data: { state: 'life'}},
    { path: 'setup', component: SetupComponent, data: { state: 'setup'}},
    { path: '**', redirectTo: '/about', pathMatch: 'full' }
  ];

export const AppRouting = RouterModule.forRoot(appRoutes, { useHash: true });
