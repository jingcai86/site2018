export class ExpPosition {
    title: string;
    company: string;
    url: string;
    location: string;
    period: string;
    detail: string[];
}
