export class ExpEducation {
    school: string;
    city: string;
    img: string;
    url: string;
    degree: string;
    description: string;
    classes: string[];
}
