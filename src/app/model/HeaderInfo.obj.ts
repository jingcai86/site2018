export class HeaderInfo {
    name: string;
    title: string;
    email: string;
    site: string;
    linkedin: string;
}
