export class WorkItem {
    title: string;
    image: string;
    description: string;
    link: string;
}
