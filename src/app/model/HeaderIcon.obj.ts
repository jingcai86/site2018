export class HeaderIcon {
    icon: string;
    disabled: boolean;
    title: string;
    url: string;
}
