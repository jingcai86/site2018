import { Component, OnInit } from '@angular/core';
import { HttpService } from '../service/http.service';
import { UtilService } from '../service/util.service';
import { HeaderInfo } from '../model/HeaderInfo.obj';
import { HeaderIcon } from '../model/HeaderIcon.obj';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  objectKeys = Object.keys;
  info: HeaderInfo;
  icons: HeaderIcon;
  loaded = false;         /* this is to act like a resolve for data, no need to create a service for just this */

  constructor(private httpService: HttpService, private util: UtilService) {}

  ngOnInit(): void {
    const calls = [
      this.httpService.getHeaderInfoContent(),
      this.httpService.getHeaderIconContent()
    ];
    forkJoin(calls).subscribe((resp: [HeaderInfo, HeaderIcon]) => {
      this.info = resp[0];
      this.icons = resp[1];
      this.loaded = true;
    });
  }

  openIcon(url: string): void {
    this.util.openUrl(url, false);
  }
}
