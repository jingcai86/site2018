import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { trigger, state, transition, animate, group, style } from '@angular/animations';
import { LifeHobby } from '../../model/LifeHobby.obj';
@Component({
  selector: 'app-life-hobby',
  templateUrl: './hobby.component.html',
  styleUrls: ['./hobby.component.scss'],
  animations: [
    trigger('slideInOut', [
        state('in', style({
            'max-height': '500px', 'opacity': '1', 'visibility': 'visible'
        })),
        state('out', style({
            'max-height': '0px', 'opacity': '0', 'visibility': 'hidden'
        })),
        transition('in => out', [group([
            animate('500ms ease-in-out', style({
                'max-height': '0px'
            })),
            animate('600ms ease-in-out', style({
                'visibility': 'hidden'
            })),
            animate('600ms ease-in-out', style({
                'opacity': '0'
            })),
          ]
        )]),
        transition('out => in', [group([
            animate('600ms ease-in-out', style({
                'visibility': 'visible'
            })),
            animate('600ms ease-in-out', style({
                'opacity': '1'
            })),
            animate('500ms ease-in-out', style({
              'max-height': '200px'
          }))
        ]
      )])
  ]),
]
})
export class HobbyComponent implements OnInit {

  @Input() hobby: LifeHobby;
  @Input() index: number;

  private showContent = 'out';

  constructor() { }

  ngOnInit() {
  }

  showHobbyContent () {
    this.showContent = 'in';
  }
  hideHobbyContent () {
    this.showContent = 'out';
  }
}
