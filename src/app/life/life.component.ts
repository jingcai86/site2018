import { Component, OnInit } from '@angular/core';
import { HttpService } from '../service/http.service';
import { LifeHobby } from '../model/LifeHobby.obj';

@Component({
  selector: 'app-life',
  templateUrl: './life.component.html',
  styleUrls: ['./life.component.scss']
})
export class LifeComponent implements OnInit {

  public areaOfInterest: LifeHobby[] = [];
  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.httpService.getPersonalLifeContent().subscribe((item: LifeHobby[]) => this.areaOfInterest = item);
  }
}
