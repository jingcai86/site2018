import KeenSlider, { KeenSliderInstance } from 'keen-slider';

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpService } from '../service/http.service';
import { About } from '../model/About.obj';
import {
  trigger,
  stagger,
  animate,
  style,
  group,
  query as q,
  transition,
  keyframes,
} from '@angular/animations';

export function query(s, a, o = { optional: true }): any {
  return q(s, a, o);
}

export const homeTransition = trigger('homeTransition', [
  transition(':enter', [
    query('.block', style({ opacity: 0 }), { optional: true }),
    query(
      '.block',
      stagger(100, [
        style({ transform: 'translateY(100px)' }),
        animate(
          '0.5s cubic-bezier(.75,-0.48,.26,1.52)',
          style({ transform: 'translateY(0px)', opacity: 1 })
        ),
      ]),
      { optional: true }
    ),
  ]),
  transition(':leave', [
    query(
      '.block',
      stagger(100, [
        style({ transform: 'translateY(0px)', opacity: 1 }),
        animate(
          '0.5s cubic-bezier(.75,-0.48,.26,1.52)',
          style({ transform: 'translateY(100px)', opacity: 0 })
        ),
      ]),
      { optional: true }
    ),
  ]),
]);

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: [
    './about.component.scss',
    '../../../node_modules/keen-slider/keen-slider.min.css'
  ],
  animations: [homeTransition],
  host: {
    '[@homeTransition]': '',
  },
})
export class AboutComponent implements OnInit {
  public content: About = { about: [], goal: [], images: [''] };
  public imgSrc = 'assets/minions/minion1.png';
  @ViewChild('sliderRef') sliderRef: ElementRef<HTMLElement>;
  slider: KeenSliderInstance = null;
  currentSlide: number = 0;
  constructor(private httpService: HttpService) {}

  ngAfterViewInit() {
    // from Keen example
    this.slider = new KeenSlider(
      this.sliderRef.nativeElement,
      {
        initial: this.currentSlide,
        loop: true,
        slideChanged: (s) => {
          this.currentSlide = s.track.details.rel;
        },
      },
      [
        (slider) => {
          let timeout;
          let mouseOver = false;
          function clearNextTimeout() {
            clearTimeout(timeout);
          }
          function nextTimeout() {
            clearTimeout(timeout);
            if (mouseOver) return;
            timeout = setTimeout(() => {
              slider.next();
            }, 5000);
          }
          slider.on('created', () => {
            slider.container.addEventListener('mouseover', () => {
              mouseOver = true;
              clearNextTimeout();
            });
            slider.container.addEventListener('mouseout', () => {
              mouseOver = false;
              nextTimeout();
            });
            nextTimeout();
          });
          slider.on('dragStarted', clearNextTimeout);
          slider.on('animationEnded', nextTimeout);
          slider.on('updated', nextTimeout);
        },
      ]
    );
  }

  ngOnInit(): void {
    this.httpService.getAboutContent().subscribe((aboutContent: About) => {
      this.content = aboutContent;
      setTimeout(() => {
        // Keen Slider doesn't work exactly right with angular due to images are dynamically loaded
        window.dispatchEvent(new Event('resize'));
      }, 100);
    });
  }

  ngOnDestroy() {
    if (this.slider) this.slider.destroy();
  }
}
