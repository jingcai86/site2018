import { Component, OnInit } from '@angular/core';
import { versions } from '../../environments/versions';
import { HttpService } from '../service/http.service';

interface SetupObj {
  environments: string[];
}

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.scss']
})
export class SetupComponent implements OnInit {
  public versions = versions;
  public setupObj: SetupObj = {
    environments: []
  };

  public displayedColumns = ['key', 'value'];

  constructor(private httpService: HttpService) { }


  ngOnInit(): void {
    this.httpService.getSetupContent().subscribe((setupResp: SetupObj) => {
      this.setupObj = setupResp;
    });
  }

}
