import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SetupComponent } from './setup.component';
import {
  MatTableModule
} from '@angular/material';

import { HttpClientModule } from '@angular/common/http';

describe('SetupComponent', () => {
  let component: SetupComponent;
  let fixture: ComponentFixture<SetupComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SetupComponent ],
      imports: [ MatTableModule, HttpClientModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
