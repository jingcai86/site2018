import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';


import { MatButtonModule } from '@angular/material/button';

import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { HttpClientModule } from '@angular/common/http';
// import { SlideshowModule } from 'ng-simple-slideshow';

import { AppComponent } from './app.component';
import { AppRouting } from './app.route.definition';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HeaderComponent } from './header/header.component';
import { AboutComponent } from './about/about.component';
import { ExperienceComponent } from './experience/experience.component';
import { WorkComponent } from './work/work.component';
import { LifeComponent } from './life/life.component';
import { SetupComponent } from './setup/setup.component';
import { PositionComponent } from './experience/position/position.component';
import { SkillComponent } from './experience/skill/skill.component';
import { EducationComponent } from './experience/education/education.component';
import { HobbyComponent } from './life/hobby/hobby.component';
import { WorkItemComponent } from './work/work-item/work-item.component';
import { IframeComponent } from './work/work-item/iframe/iframe.component';

// For fixing iframe auto refresh issue regarding to unsafe url
// this is to create a "safe" pipe
@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        AboutComponent,
        ExperienceComponent,
        WorkComponent,
        LifeComponent,
        SetupComponent,
        PositionComponent,
        SkillComponent,
        EducationComponent,
        HobbyComponent,
        WorkItemComponent,
        IframeComponent,
        SafePipe
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppRouting,
        MatButtonModule,
        MatCheckboxModule,
        MatChipsModule,
        MatTabsModule,
        MatExpansionModule,
        MatTableModule,
        MatDialogModule,
        MatTooltipModule,
        MatProgressSpinnerModule,
        // SlideshowModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}

