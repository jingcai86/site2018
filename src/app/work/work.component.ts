import { Component, OnInit } from '@angular/core';
import { HttpService } from '../service/http.service';
import { WorkItem } from '../model/WorkItem.obj';
@Component({
  selector: 'app-work',
  templateUrl: './work.component.html',
  styleUrls: ['./work.component.scss']
})
export class WorkComponent implements OnInit {
  public workItems: WorkItem[];
  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.httpService.getWorkContent().subscribe((items: WorkItem[]) => this.workItems = [...items, { 
      title: 'More examples to come...',
      image: '',
      description: '',
      link: '',
    } as WorkItem]);
  }

}
