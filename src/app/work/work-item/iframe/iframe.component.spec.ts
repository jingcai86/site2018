import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { IframeComponent } from './iframe.component';
import {
  MatProgressSpinnerModule
} from '@angular/material/progress-spinner';

import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material/dialog';

import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'safe'})
class SafePipe implements PipeTransform {
    transform(value: number): number {
        // blah blah
        return value;
    }
}

describe('IframeComponent', () => {
  let component: IframeComponent;
  let fixture: ComponentFixture<IframeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ IframeComponent, SafePipe ],
      imports: [ MatProgressSpinnerModule, MatDialogModule],
      providers: [
        // workaround: why I can't inject MatDialogRef in the unit test?
        { provide: MatDialogRef, useValue: {} },
        { provide: MAT_DIALOG_DATA, useValue: {} },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IframeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
