import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  templateUrl: './iframe.component.html',
  styleUrls: ['./iframe.component.scss']
})
export class IframeComponent implements OnInit {
  public isLoading = true;
  constructor(public dialogRef: MatDialogRef<IframeComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {}


  ngOnInit(): void {
    const eventMethod = window.addEventListener ? 'addEventListener' : 'attachEvent';
    const eventer = window[eventMethod];
    const messageEvent = eventMethod === 'attachEvent' ? 'onmessage' : 'message';

    eventer(messageEvent, e => {
      if (e.data === 'ready' || e.message === 'ready' ) {
        this.isLoading = false;
      }
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
