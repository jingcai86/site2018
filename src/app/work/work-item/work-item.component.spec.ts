import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WorkItemComponent } from './work-item.component';

import {
  MatDialogModule
} from '@angular/material/dialog';

describe('WorkItemComponent', () => {
  let component: WorkItemComponent;
  let fixture: ComponentFixture<WorkItemComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkItemComponent ],
      imports: [ MatDialogModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkItemComponent);
    component = fixture.componentInstance;
    component.detail = {
      'title': 'a',
      'description': 'b',
      'image': 'c',
      'link': 'd'
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
