import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { WorkItem } from '../../model/WorkItem.obj';
import { IframeComponent } from './iframe/iframe.component';
@Component({
  selector: 'app-work-item',
  templateUrl: './work-item.component.html',
  styleUrls: ['./work-item.component.scss']
})
export class WorkItemComponent {
  @Input() detail: WorkItem;
  
  constructor(public dialog: MatDialog) { }

  openIframe(url): void {
    if (url) {
      const dialogRef = this.dialog.open(IframeComponent, {
        width: '90%',
        height: '90%',
        data: { link: url }
      });
  
      /*
      dialogRef.afterClosed().subscribe(result => {
  
      });
      */
    }

  }

}
