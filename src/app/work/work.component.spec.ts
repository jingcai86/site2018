import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WorkComponent } from './work.component';
import { WorkItemComponent } from './work-item/work-item.component';
import { HttpClientModule } from '@angular/common/http';

describe('WorkComponent', () => {
  let component: WorkComponent;
  let fixture: ComponentFixture<WorkComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkComponent, WorkItemComponent ],
      imports: [ HttpClientModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
