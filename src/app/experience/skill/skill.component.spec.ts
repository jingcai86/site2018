import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SkillComponent } from './skill.component';

import { MatExpansionModule } from '@angular/material/expansion';

import { MatChipsModule } from '@angular/material/chips';


describe('SkillcategoryComponent', () => {
  let component: SkillComponent;
  let fixture: ComponentFixture<SkillComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillComponent ],
      imports: [ MatExpansionModule, MatChipsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
