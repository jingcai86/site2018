import { Component, OnInit, Input } from '@angular/core';
import { ExpSkill } from '../../model/ExpSkill.obj';
import { UtilService } from '../../service/util.service';
@Component({
  selector: 'app-exp-skill',
  templateUrl: './skill.component.html',
  styleUrls: ['./skill.component.scss']
})
export class SkillComponent implements OnInit {
  private MAX_CATEGORY_ONCE = 2;
  private categoryOpened: boolean[] = [
    true,
    true,
    false,
    false,
    false,
    false
  ];
  @Input() skills: ExpSkill[];
  constructor(private util: UtilService) {}

  ngOnInit() {}

  openingCategory(idx: number) {
    this.categoryOpened[idx] = true;

    const count: number = this.categoryOpened.reduce((total, cur) => total + (cur === true ? 1 : 0), 0);

    // If number of tabs is greater than MAX_CATEGORY_ONCE, close the first opened one;
    // Better approach should combine reduce and findIndex to gain performance(nlogn + n);
    // In this case, however, may not be necessary
    if (count > this.MAX_CATEGORY_ONCE) {
      const found: number = this.categoryOpened.findIndex((categoryOpened, index) => categoryOpened === true && index !== idx );
      this.categoryOpened[found] = false;
    }
  }

  closingCategory(idx: number) {
    this.categoryOpened[idx] = false;
  }

  openUrl(skill: string) {
    this.util.openUrl(skill, true);
  }

}
