import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ExperienceComponent } from './experience.component';

import { PositionComponent } from './position/position.component';
import { SkillComponent } from './skill/skill.component';
import { EducationComponent } from './education/education.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatTabsModule,
  MatButtonModule,
  MatCheckboxModule,
  MatChipsModule,
  MatExpansionModule,
  MatTableModule,
  MatDialogModule,
  MatTooltipModule
} from '@angular/material';

import { HttpClientModule } from '@angular/common/http';

describe('ExperienceComponent', () => {
  let component: ExperienceComponent;
  let fixture: ComponentFixture<ExperienceComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ExperienceComponent, PositionComponent, SkillComponent, EducationComponent ],
      imports: [
        MatTabsModule,
        MatButtonModule,
        MatCheckboxModule,
        MatChipsModule,
        MatExpansionModule,
        MatTableModule,
        MatDialogModule,
        MatTooltipModule,
        HttpClientModule,
        BrowserAnimationsModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperienceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
