import { Component, OnInit } from '@angular/core';
import { ExpTab } from '../model/ExpTab.enum';
import { ExpPosition } from '../model/ExpPos.obj';
import { ExpSkill } from '../model/ExpSkill.obj';
import { ExpEducation } from '../model/ExpEdu.obj';
import { HttpService } from '../service/http.service';


import { sequence, trigger, stagger, animate, style, group, query as q, transition, keyframes, animateChild } from '@angular/animations';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({opacity: 0}),
        animate(500, style({opacity: 1}))
      ])
    ])
  ]
})
export class ExperienceComponent implements OnInit {
  public selectedTab: ExpTab;
  public tab: any;
  public positions: ExpPosition[] = [];
  public skills: ExpSkill[] = [];
  public educations: ExpEducation[] = [];
  public tabImg: string;

  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.tab = ExpTab;
    this.onSwitchTab(0);

    // API call is made here so ngIfs wouldn't pull API again if sub-tab is switched
    this.httpService.getPositionContent().subscribe((resp: ExpPosition[]) => this.positions = resp);
    this.httpService.getSkillContent().subscribe((resp: ExpSkill[]) => this.skills = resp);
    this.httpService.getEducationContent().subscribe((resp: ExpEducation[]) => this.educations = resp);
  }


  onSwitchTab(tab: ExpTab) {
    this.selectedTab = tab;
    this.tabImg = '../../assets/minions/exp' + this.selectedTab + '.png';

  }
}
