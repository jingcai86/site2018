import { Component, OnInit, Input } from '@angular/core';
import { ExpEducation } from '../../model/ExpEdu.obj';
@Component({
  selector: 'app-exp-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit {

  @Input() educations: ExpEducation[] = [];

  displayedColumns: string[] = ['id', 'name'];

  constructor() { }

  ngOnInit() {
  }

}
