import { Component, OnInit, Input } from '@angular/core';
import { ExpPosition } from '../../model/ExpPos.obj';

@Component({
  selector: 'app-exp-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.scss']
})
export class PositionComponent implements OnInit {

  @Input() positions: ExpPosition[];

  constructor() { }

  ngOnInit() {
    window.onload = function() {
      const vids: any = document.getElementsByClassName('pos-bg-video');
      for (let i = 0; i < vids.length; i++) {
        vids[i].playbackRate = 0.5;
      }
    };
  }
}
